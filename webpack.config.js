const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HandlebarsPlugin = require('handlebars-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const path = require('path');

const paths = {
    src: {
        images: './src/assets/images',
        js: './src/assets/js',
        css: './src/assets/css',
    },
    dist: {
        images: './assets/images',
        js: './assets/js',
        css: './assets/css',
    },
};

module.exports = {
    devtool: 'source-map',
    entry: {
        main: [paths.src.js + '/main.js', paths.src.css + '/main.css'],
    },
    output: {
        filename: paths.dist.js + '/[name].bundle.js',
    },
    mode: 'development',
    module: {
        rules:  [
                    {
                        test: /\.css$/i,
                        include: path.resolve(__dirname, paths.src.css.slice(2)),
                        use: [
                            {
                                loader: MiniCssExtractPlugin.loader,
                            },
                            {
                                loader: 'css-loader',
                                options: {
                                    url: false,
                                },
                            },
                        ],
                    },
        ],
    },
    plugins: [
        new CopyPlugin({
            patterns: [
                {
                    from: paths.src.images,
                    to: paths.dist.images,
                },
            ],
        }),
        new HandlebarsPlugin({
            entry: path.join(process.cwd(), 'src', '*.html'),
            output: path.join(process.cwd(), 'dist', '[name].html'),
            partials: [path.join(process.cwd(), 'src', 'layouts', '**', '*.html')],
            helpers: {
                webRoot: function () {
                    return '{{webRoot}}';
                },
            },
            onBeforeSave: function (Handlebars, resultHtml, filename) {
                const level = filename.split('//').pop().split('/').length;
                const finalHtml = resultHtml.split('{{webRoot}}').join('.'.repeat(level));

                return finalHtml;
            },
        }),
        new MiniCssExtractPlugin({
            filename: paths.dist.css + '/[name].bundle.css',
        }),
    ],
    target: 'web',
};