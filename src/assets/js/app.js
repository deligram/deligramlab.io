feather.replace();

$(document).ready(function() {
    $("input#intlTelPhone").intlTelInput({
        initialCountry: "my",
        onlyCountries: ["my"],
        allowDropdown: false,
        utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.4.6/js/utils.js",
    });

    // Plus quantity on click
    $('#product_plus_quantity').on('click', function(){
        plusQuantity();
    });

    // Minus quantity on click
    $('#product_minus_quantity').on('click', function(){
        minusQuantity();
    });

    // Quantity input box is on type in value
    $('#product_input_quantity').on('keyup', function(e){
        inputQuantity(e);
    });
});

function plusQuantity() {
    var inputQuantityBox = $("#product_input_quantity"),
        inputQuantityValue = parseInt($("#product_input_quantity").val()),
        inputValue = 1,
        maxValue = 99;

        inputValue = inputQuantityValue + 1;

    if(inputQuantityValue < maxValue) {
        inputQuantityBox.attr("value", inputValue);
        inputQuantityBox.val(inputValue);
    }
}

function minusQuantity() {
    var inputQuantityBox = $("#product_input_quantity"),
        inputQuantityValue = parseInt($("#product_input_quantity").val()),
        inputValue = 1;
        minValue = 1;

        inputValue = inputQuantityValue - 1;

    if(inputQuantityValue > minValue) {
        inputQuantityBox.attr("value", inputValue);
        inputQuantityBox.val(inputValue);
    }
}

function inputQuantity(e) {
    var inputQuantityBox = $("#product_input_quantity"),
        inputQuantityValue = parseInt($("#product_input_quantity").val()),
        inputValue = 1,
        minValue = 1,
        maxValue = 99;

        inputValue = inputQuantityBox.val();
        inputValue = inputValue.replace(/[^0-9\.]/g,'');
        inputValue = parseInt(inputValue);

    if(inputQuantityBox.val()) {
        if(inputValue < maxValue) {
            inputQuantityBox.attr("value", inputValue);
            inputQuantityBox.val(inputValue);
        } else {
            inputQuantityBox.attr("value", minValue);
            inputQuantityBox.val(minValue);
        }
    }
}