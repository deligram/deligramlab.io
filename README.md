# Deligram

Deligram system made with :heart: by Shoppegram.

## How to run :runner:

Before run make sure you delete the node_modules file first.

1. Then type the command {+ npm install +} at your terminal

2. Next, to start see live changes at your localhost you can run this command {+ npm run watch +}

3. Before you commit the code to Gitlab, make sure you run this command to compile your code {+ npm run build +}

## How to use/code :computer:

- Edit all of the files inside {+ src folder only +}.
- All the header & menu html you can find inside {+ src/layouts +}
- All CSS, JS and images you can find it inside {+ src/assets +}
- If you add new CSS or JS file, make sure you import it inside their respective main files at the src folder.

> Thats all about this repository, if got any problems or issues can DM me. :bow: